# Publications

## Research articles

[A62] The 1965 Mahavel Landslide (Réunion Island, Indian Ocean): Morphology, Volumes, Flow Dynamics, and Causes of a Rock Avalanche in Tropical Setting
Michon, L., Gayer, E., Lucas, A., Bellin, F., Gougeon, M.
Journal of Geophysical Research: Earth Surface, doi: 10.1029/2022JF006944, 2023

[A61] Phenomenological model of suspended sediment transport in a small river catchment
Roque-Bernard* A., A. Lucas, E. Gayer, P. Allemand, C. Dessert, and E. Lajeunesse
Earth Surface Dynamics, doi:10.5194/esurf-11-363-2023, 2023

[A60] Permafrost molards as an analogue for ejecta-ice interactions at Hale Crater, Mars,
Morino C., Conway S. , Philippe M., Peignaux C., Svennevig K., Lucas A., et al.,
Icarus, doi:10.1016/j.icarus.2022.115363, 2022


[A59] Uncovering a 70-year-old permafrost- degradation induced disaster in the Arctic, the 1952 Niiortuut landslide-tsunami in central West Greenland
K. Svennevig, M. Keiding, N. J. Korsgård, A. Lucas, et al.,
Science of the total environment, doi:10.1016/j.scitotenv.2022.160110, 2022

[A58] Composition, Roughness, and Topography from Radar Backscatter at Selk Crater, the Dragonfly Landing Site
Bonnefoyfi , L.E., A. Lucas, A. G. Hayes, D. Lalich, V. Poggiali, S. Rodriguez, R. Lorenz, A. Le Gall
The Planetary Science Journal, doi:10.3847/psj/ac8428, 2022

[A57] Seismic sources of InSight marsquakes and seismotectonic context of Elysium Planitia, Mars
Jacob. A*, ..., Lucas A., et al.
Tectonophysics, doi:10.1016/j.tecto.2022.229434, 2022

[A56] Morphometry and segmentation of the Cerberus Fossae fault system, Mars: implications on marsquake properties in Elysium Planitia
Perrin, C., Jacob, A.*, Lucas, A., Batov, A., Gudkova, T., Rodriguez, S., Lognonné, P., Stevanović, J., Drilleau, M. & Fuji, N.
JGR Planets,doi:10.1029/2021JE007118, 2022

[A55] Science goals and new mission concepts for a future exploration of Titan’s atmosphere, geology and habitability: Titan POlar Scout/orbiteEr and In situ lake lander and DrONe explorer (POSEIDON)
Rodriguez S., ..., Lucas, A., et al.
Experimental Astronomy, arXiv:2110.10466, 2021

[A54] Decennial geomorphic transport from archived time series digital elevation models: a cookbook for tropical and alpine environments
Lucas, A. & E. Gayer
IEEE Geoscience and Remote Sensing Magazine, doi:10.1109/MGRS.2021.3121370, TechRxiv:10.36227/techrxiv.14828256, 2021

[A53] Thermal Emission of Saturn Icy MoonsImpact of Topography and Regolith Properties
C. Ferrari, A. Lucas, S. Jacquemoud
A&A, 2021

[A52] Seasonal Seismic Activity on Mars
Knapmeyer, M., S. C. Stähler, ... A. Lucas, et al.,
EPSL, 2021

[A51] Dynamics of recent landslides (<20 My) on Mars: Insights from high-resolution topography on Earth and Mars and numerical modelling
Guimpier A., S. J. Conway 1 , A. Mangeney, A. Lucas, et al.,
Planetary and Space Science,doi:10.1016/j.pss.2021.105303, 2021

[A50] Vortex-dominated aeolian activity at InSight's landing site, Part 1: Multi-instrument Observations, Analysis and Implications
Charalambous C., ..., Lucas A., et al.,
JGR Planets, doi:10.1029/2020JE006757, 2021

[A49] Topography curvature effects in thin-layer models for gravity-driven flows without bed erosion
Peruzzetto M., ..., Lucas, A.
Journal of Geophysical Research - Earth Surface., doi:10.1029/2020JF005657, 2021

[A48] Operational estimation of landslide runout: comparison of empirical and numerical methods
Peruzzetto M., ..., Lucas, A.
Geosciences, doi:10.3390/geosciences1010005, 2020

[A47] A new digital terrain model of the Huygens landing site on Saturn’s largest moon, Titan
Daudon C.*, Lucas, A., et al.,
Earth and Space Science, doi:10.1029/2020EA001127, 2020

[A46] A New Crater Near InSight: Implications for Seismic Impact Detectability on Mars
Daubard I., ..., Lucas, A., et al.
JGR-E, doi:10.1029/2020JE006382, 2020

[A45] Monitoring of Dust Devil Tracks Around the InSight Landing Site, Mars, and Comparison with in-situ Atmospheric Data
Perrin, C., S. Rodriguez, A. Jacob*, Lucas, A., et al.,
Geophysical Research Letters, 2020GL087234, 2020

[A44] The Seismicity of Mars
Giardini, D., ..., Lucas, A., et al.,
Nature Geoscience, doi:10.1038/s41561-020-0539-8, 2020

[A43] An overview of the initial results on atmospheric science from InSight measurements.
Banfield, D., ..., Lucas, A., et al.,
Nature Geoscience, NGS-2019-08-7066347A, 2020

[A42] Crust stratigraphy and heterogeneities of the first kilometers at the dichotomy boundary in western Elysium Planitia and implications for InSight lander
L. Pan, C. Quantin, B. Tauzin, C. Michaut, M. Golombek, P. Lognonné, P. Grindrod, B. Langlais, T. Gudkova, I. E. Stepanova, S. Rodriguez and A. Lucas
Icarus, doi: 10.1016/j.icarus.2019.113511, 2019

[A41] Texture and composition of Titan's equatorial sand seas inferred from Cassini SAR data: Implications for aeolian transport and dune morphodynamics
A. Lucas, S. Rodriguez, F. Lemonnier*, A. Le Gall, S. MacKenzie, C. Ferrari, Ph. Paillou, C. Narteau
Journal of Geophys. Research Planets, doi:10.1029/2019JE005965, arXiv:1702.02881, 2019

[A40] Titan as revealed by the Cassini RADAR
Lopes, R., ... A. Lucas, et al.,
Space Science Review, doi:doi.org/10.1007/s11214-019-0598-6, 2019

[A39] The case for seasonal surface changes at Titan’s lake district
MacKenzie, S. Barnes, Hofgartner, Birch, Hedman, A. Lucas, Rodriguez, Turtle, Sotin
Nature Astronomy, doi:10.1038/s41550-018-0687-6, 2019

[A38] Deep-seated gravitational deformation scaling on Earth and Mars: implications for paraglacial slope stability and driving forces
Kromuszczynska, O., ... A. Lucas
Earth Surf. Dynam., doi:10.5194/esurf-7-361-2019, 2019

[A37] Persistence of intense, climate-driven runoff late in Mars history
Kite, E.S., D. P. Mayer, S. A. Wilson, J. M. Davis, A. Lucas and G. Stucky de Quay
Science Advances, doi:10.1126/sciadv.aav7710, 2019

[A36] Empirical investigation of friction weakening of terrestrial and Martian landslides using discrete element models
Borykov, T., D. Mège, A. Mangeney, P. Richard, J. Gurgurewicz, A. Lucas
Landslide doi: 10.1007/s10346-019-01140-8, 2019

[A35] OZCAR: The French network of critical zone observatories
Gaillardet, J., ... Lucas A. et al.,
Vadose Zone Journal, doi: 10.2136/vzj2018.04.0067, 2018

[A34] Impact-seismic investigations of the InSight mission
Daubar,I, ... Lucas A. et al.,
Space Science Reviews, doi: 10.1007/s11214-018-0562-x, 2018

[A33] Atmospheric Science with InSight
Spiga, A., ... Lucas A. et al.,
Space Science Reviews, doi:10.1007/s11214-018-0543-0, 2018

[A32] Observational evidence for active dust storms on Titan at equinox
Rodriguez S., ... A. Lucas et al.,
Nature Geoscience, doi:10.1038/s41561-018-0233-2, 2018

[A31] Geology and Physical Properties Investigations by the InSight Lander
Golombek M,... A. Lucas et al.,
Space Sci. Rev., doi:10.1007/s11214-018-0512-7, 2018

[A30] Geological Evolution of Titan's Equatorial Regions: Possible Nature and Origin of the Dune Material
Brossier J,... A. Lucas et al.,
JGR-Planets, doi:10.1029/2017JE005399, 2018

[A29] First quantification of relationship between dune orientation and sediment availability, Olympia Undae, Mars
Fernandez-Cascales. L.*, A. Lucas, S. Rodriguez, X. Gao, A. Spiga, C. Narteau
EPSL, doi:10.1016/j.epsl.2018.03.001, 2018

[A28] The warm subsurface of Enceladus' South Polar Terrain Thermal anomalies reveal deformations of a thin ice shell at Enceladus' South Pole
A. Le Gall, ... A. Lucas et al.,
Nature Astronomy, doi:10.1038/s41550-017-0063, 2017

[A27] Low thermal inertias of icy planetary surfaces: Evidence for amorphous ice?
C. Ferrari and A. Lucas
Astronomy and Astrophysics, doi:10.1051/0004-6361/201527625, 2016

[A26] Variations in Titan’s dune orientations as a result of orbital forcing
McDonald, G.D., ... A. Lucas et al.,
Icarus doi:10.1016/j.icarus.2015.11.036, 2016

[A25] Compositional and spatial variations in Titan dune and interdune regions from Cassini VIMS and RADAR
Bonnefoy, L.E., ... A. Lucas
Icarus, doi:10.1016/j.icarus.2015.09.014, 2015

[A24] Sediment flux from the morphodynamics of elongating linear dunes
A. Lucas, C. Narteau, S. Rodriguez et al.
Geology, doi:10.1130/G37101.1, 2015

[A23] Resolving the era of river-forming climates on Mars using stratigraphic logs of river-deposit dimensions
Kite E., Howard A., A. Lucas, K. Lewis
Icarus, doi:10.1016/j.epsl.2015.03.019, 2015

[A22] Stratigraphy of Aeolis Dorsa, Mars: stratigraphic context of the great river deposits
Kite E.S., Howard, A, Lucas A., Armstrong J.C., Aharonson O., and M.P. Lamb
Icarus, doi:10.1016/j.icarus.2015.03.007, 2015

[A21] Methane storms as a driver of Titan's dune orientation
B. Charnay, E. Barth, S. Rafkin, C Narteau, S. Lebonnois, S. Rodriguez, S. Courrech du Pont, A. Lucas
Nature Geoscience, doi:10.1038/ngeo2406, 2015

[A20] Sand dune patterns on Titan controlled by long-term climate cycles
RC Ewing, AG Hayes, A Lucas
Nature Geoscience, doi:10.1038/ngeo2323, 2015

[A19] Threshold for sand mobility on Mars calibrated from seasonal variations of sand flux
F. Ayoub, J-P. Avouac, C. Newman, M. Richardson, A. Lucas, S. Leprince, and N. Bridges
Nature Comm. doi:10.1038/ncomms6096, 2014

[A18] Growth mechanisms and dune orientation on Titan
A. Lucas, S. Rodriguez, C. Narteau, B. Charnay, T. Tokano, A. Garcia, M. Thiriet*, S. Courrech du Pont, A. Hayes, R. Lorenz, O. Aharonson
Geophys. Research Letters, doi:10.1002/2014GL060971, 2014

[A17] Insights into Titan''s geology and hydrology based on enhanced image processing of Cassini RADAR data
A. Lucas, O. Aharonson, C-A. Deledalle, A. Hayes, R. Kirk, E. Howington-Kraus
Journal of Geophys. Research, doi:10.1002/2013JE004584, 2014

[A16] A Radar Map of Titan Seas: Tidal Dissipation and Ocean Mixing through the Throat of Krakens
R D. Lorenz, R. L. Kirk, A. G. Hayes, Y. Z. Anderson, J. I. Lunine, T. Tokano, E. P. Turtle, M. J. Malaska, J. Soderblom, A. Lucas, F. Nimmo, O. Karatekin, S. D. Wall
Icarus, doi:10.1016/j.icarus.2014.04.005, 2014

[A15] Low palaeopressure of the martian atmosphere estimated from the size distribution of ancient craters
E.S. Kite, J-P. Williams, A. Lucas and O. Aharonson
Nature Geoscience, doi:10.1038/ngeo2137, 2014

[A14] Frictional velocity-weakening in landslides on Earth and on other planetary bodies
A. Lucas, A. Mangeney, J-P. Ampuero
Nature Comm., 5:3417, doi:10.1038/ncomms4417, 2014

[A13] A Global Topographic Map of Titan
R. Lorenz, B. W. Stiles, O. Aharonson, A. Lucas, A. G. Hayes, R. L. Kirk, H. A. Zebker, E. P. Turtle, F. Nimmo, C. D. Neish, J. W. Barnes, E. R. Stofan
Icarus, doi:10.1016/j.icarus.2013.04.002, 2013

[A12] Pacing Early Mars river activity: Embedded craters in the Aeolis Dorsa region imply river activity spanned >(1-20) Myr
E.S. Kite, A. Lucas, C.I. Fassett
Icarus, doi:10.1016/j.icarus.2013.03.029, 2013

[A11] Global mapping and characterization of Titan’s dune fields with Cassini: correlation between RADAR and VIMS observations
Rodriguez S., A. Garcia, A. Lucas, T. Appéré, A. Le Gall, E. Reffet, L. Le Corre, S. Le Mouélic, T. Cornet, S. Courrech du Pont, C. Narteau, O. Bourgeois, J. Radebaugh, K. Arnold, J.W. Barnes, C. Sotin, R.H. Brown, R.D. Lorenz, E.P. Turtle
Icarus, doi:10.1016/j.icarus.2013.11.017, 2013

[A10] Earth-like Sand Fluxes on Mars
N.T. Bridges, F. Ayoub, J-P. Avouac, S. Leprince, A. Lucas, S. Mattson
Nature, doi:10.1038/nature11022, 2012

[A9] Morphological and mechanical characterization of past and present flow activities in a periglacial environment: the case of the Russell dune (Mars)
G. Jouannic, J. Gargani, F. Costard, G. G. Ori, C. Marmo, F. Schmidt, A. Lucas
Planetary and Space Science, doi:10.1016/j.pss.2012.07.005, 2012

[A8] On the run out distance of geophysical gravitational flows: insight from fluidized granular collapse experiments
O. Roche, M. Attali, A. Mangeney, A. Lucas
Earth Planet. Sci. Lett., doi:10.1016/j.epsl.2011.09.023, 2011

[A7] Influence of Scar Geometry on landslide dymanics and deposits: Application to Martian landslides
A. Lucas, A. Mangeney, D. Mège, F. Bouchut
J. Geophys. Res., doi:10.1029/2011JE003803, 2011

[A6] Stratigraphy, mineralogy, and origin of layered deposits inside Terby crater, Mars
V. Ansan, D. Loizeau, N. Mangold, S. Le Mouélic, J. Carter, F. Poulet, G. Dromart, A. Lucas, J- P. Bibring, A. Gendrin, B. Gondet, Y. Langevin, Ph. Masson, S. Murchie, J. Mustard, G. Neukum
Icarus, doi:10.1016/j.icarus.2010.09.011, 2011

[A5] Numerical modeling of landquake
P. Favreau, A. Mangeney, A. Lucas, G. Crosta, and F. Bouchut
Geophys. Res. Lett., doi:10.1029/2010GL043512, 2010

[A4] Erosion and mobility in granular collapse over sloping beds
A. Mangeney, O. Roche, O. Hungr, N. Mangold, G. Faccanoni, A. Lucas
J. Geophys. Res., doi:10.1029/2009JF001462, 2010

[A3] Sinuous gullies on Mars: Frequency, distribution and implications for flow properties
N. Mangold, A. Mangeney, V. Migeon, V. Ansan, A. Lucas, F. Bouchut, D. Baratoux
J. Geophys. Res., doi:10.1029/2009JE003540, 2010

[A2] New insight on genetic links between outflows and chasmata on Valles Marineris plateau, Mars
A. Lucas, V. Ansan, N. Mangold
Géomorphologie: relief, processus, environnement, doi:10.4000/geomorphologie.7485, 2009.

[A1] Mobility and topographic effects for large Valles Marineris landslides on Mars
A. Lucas, and A. Mangeney
Geophys. Res. Lett., doi:10.1029/2007GL029835, 2007

## Proceedings and others

[B8] POPS: An Efficient Framework for GPU-based Feature Extraction of Massive Gridded Planetary LiDAR Data [Scalable Data Science]
Liang S., Palpanas T., Lucas A.
in review for PVLDB, doi:10.13140/RG.2.2.20832.76800

[B7] Sentinel-HR Phase 0 Report
Michel, J, Hagolle, O.,.... A. Lucas, et al.,
Research Report CNES, ⟨hal-03643411⟩, 2022

[B6] Geomorphic transport from historical shape from motion: Implications for tropical and alpine environments
A. Lucas, E Gayer
ESSOar: doi:10.1002/essoar.10501233.1

[B5]Science goals and mission concepts for a future orbital and in situ exploration of Titan
Rodriguez, S., ..., A. Lucas, et al.,
arXiv:1908.01374 [astro-ph.EP], 2019

[B4] Titan's Surface Geology, in: I. Mueller-Wodarg, C.G., T. Cravens and E. Lellouch (Ed.), Titan: Surface, Atmosphere and Magnetosphere
Aharonson, O., Hayes, A., Lopes, R., A. Lucas, Hayne, P., Perron, J.T.
Cambridge University Press, ISBN:9780521199926, 2013

[B3] Slippery Sliding on Icy Iapetus
A. Lucas
Nature Geoscience, News and Views, doi:10.1038/ngeo1532, 2012

[B2] Utilisation des ondes sismiques pour comprendre et contraindre la dynamique des effondrements gravitaires
A. Mangeney, P. Favreau, L. Moretti, A. Lucas, G. Crosta
Comptes Rendus du 20e Congres Francais de Mecanique, Besancon, 2011

[B1] Benchmark Excercices for Granular Flows
A. Lucas, A. Mangeney, Bouchut F., Bristeau, M.O., Mege, D.
Proceedings of the 2007 International Forum on Landslide Disaster Management, Hong-Kong, Vol.2, ISBN: 9627619302, 2007
